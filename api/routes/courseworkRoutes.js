'use strict';

module.exports = function(app) {
  var courseworkList = require('../controllers/courseworkController');
  var jwtauth = require('../config/jwtauth.js');

  app.route('/api/coursework')
    .get(jwtauth,courseworkList.listAllCourseworks)
    .post(jwtauth,courseworkList.createCoursework);

  app.route('/api/courseworks/:id')
    .get(jwtauth,courseworkList.getCoursework)
    .put(jwtauth,courseworkList.updateCoursework)
    .delete(jwtauth,courseworkList.deleteCoursework);

};
