'use strict';
module.exports = function(app) {
    var userCont = require('../controllers/userController');
    var jwtauth = require('../config/jwtauth.js');
    
    app.route('/api/login')
    .post(userCont.login);

    app.route('/api/createUser')
    .post(userCont.createUser);

    app.route('/api/getAllUsers')
    .get(jwtauth,userCont.listAllUsers);

    app.route('/api/user/:id')
    .get(jwtauth,userCont.getUser)
    .delete(jwtauth,userCont.deleteUser);


    app.route('/api/logout')
    .post(userCont.logout);

}

