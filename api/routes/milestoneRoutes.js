'use strict';
module.exports = function(app) {
  var milestoneList = require('../controllers/milestoneController');
  var jwtauth = require('../config/jwtauth.js');

  app.route('/api/milestone')
    .get(jwtauth,milestoneList.listAllMilestones)
    .post(jwtauth,milestoneList.createMilestone);

  app.route('/api/milestone/:id')
    .get(jwtauth,milestoneList.getMilestone)
    .put(jwtauth,milestoneList.updateMilestone)
    .delete(jwtauth,milestoneList.deleteMilestone);

};