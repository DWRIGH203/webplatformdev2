'use strict';

var mongoose = require('mongoose'),
Coursework = mongoose.model('Courseworks');
var userCont = require('./userController');
var milestoneCont = require('./milestoneController');


exports.listAllCourseworks = function(req, res) {
  console.log("authenticated stauts:" + req.isAuthenticated());
  Coursework.find({}, function(err, coursework) {
    res.json(coursework);
    res.status(200);
      
    if (err) {
      res.status(500);
      console.log(err)
    }
  });
};

exports.createCoursework = function(req, res) {
  console.log(req.body.coursework)
  var newCoursework = new Coursework(req.body.coursework);
  newCoursework.save(function(err, coursework) {
    if (err) {
      res.status(500);
      console.log(err)
    }

    console.log("Coursework Successfully Added!");

  });
  
   userCont.getUserObj(newCoursework.user, function(user) {
   user.coursework.push(newCoursework);
   user.save();
  });

    res.json(newCoursework);
    res.status(200);    
};

exports.getCoursework = function(req, res) {
  console.log('Req params', req.params) 
  Coursework.find({ '_id': req.params.id}, function(err, coursework) {
    console.log('This coursework was found', coursework)
    res.json(coursework);
    res.status(200);

    if (err) {
      res.status(500);
      console.log(err)
    }
}).populate({ path: 'milestones' });
};

exports.updateCoursework = function(req, res) {
  Coursework.updateOne({ '_id': req.params.id }, req.body.coursework, {new: true, useFindAndModify: false}, function(err, coursework) {
    res.json(coursework);
    res.status(200);
      
    if (err) {
      res.status(500);
      console.log(err)
    }
    console.log("Coursework Updated Successfully!");
  });
};

exports.deleteCoursework = function(req, res) {

milestoneCont.deleteMilestoneByCoursework(req.params.id);
Coursework.deleteOne(
  { '_id': req.params.id }, function(err, coursework) {
 
    res.json(coursework);
    res.status(200);
      
    if (err) {
      res.status(500);
      console.log(err)
    }
    
    console.log("Coursework Successfully Deleted");

  })
};

  exports.getCourseworkObj = function(id, res) {
    console.log('id inside CW controller ' + JSON.stringify(id));

    Coursework.findOne(
      { '_id': id }, function(err, coursework) {   

      res(coursework);
      
      if (err) {
        res.status(500);
        console.log(err)
      }

      console.log("Found Coursework with id: " + coursework._id);
    })
  }; 