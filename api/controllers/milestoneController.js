'use strict';

var mongoose = require('mongoose'),
Milestone = mongoose.model('Milestones');
var courseworkCont = require('../controllers/courseworkController');

exports.listAllMilestones = function(req, res) {
  Milestone.find({}, function(err, milestone) {
    res.json(milestone);
    res.status(200);
      
    if (err) {
      res.status(500);
      console.log(err)
    }
  });
};

exports.createMilestone = function(req, res) {
  var newMilestone = new Milestone(req.body.milestone);  
  newMilestone.save(function(err, milestone) {
    if (err) {
      res.status(500);
      console.log(err)
    }

    console.log("Milestone Successfully Added!");
  
  });
    courseworkCont.getCourseworkObj(newMilestone.coursework, function(coursework) {
      coursework.milestones.push(newMilestone);
      coursework.save();
    });

    res.json(newMilestone);
    res.status(200);
};

exports.getMilestone = function(req, res) {
  Milestone.find({ '_id': req.params.id}, function(err, milestone) {
    res.json(milestone);
    res.status(200);

    if (err) {
      res.status(500);
      console.log(err)
    }
});
};

exports.updateMilestone = function(req, res) {
  Milestone.updateOne({ '_id': req.params.id }, req.body.milestone, {new: true, useFindAndModify: false}, function(err, milestone) {
    console.log('The  milestone passed in');
    console.log(req.body.milestone)
    console.log(milestone);
    res.json(milestone);
    res.status(200);
      
    if (err) {
      res.status(500);
      console.log(err)
    }

    console.log("Milestone Updated Successfully!");

  });
};

exports.deleteMilestone = function(req, res) {
Milestone.deleteOne(
  { '_id': req.params.id }, function(err, milestone) {
 
    res.json(milestone);
    res.status(200);
      
    if (err) {
      res.status(500);
      console.log(err)
    }
    
    console.log("Milestone Successfully Deleted");

  })
};

exports.deleteMilestoneByCoursework = function(courseworkId) {
  Milestone.deleteMany(
    { 'coursework': courseworkId }, function(err, milestone) {
   
      // res(milestone);
        
      if (err) {
        res.status(500);
        console.log(err)
      }
      
      console.log("Milestone Successfully Deleted");
  
    })
  };
