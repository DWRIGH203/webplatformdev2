'use strict';

var mongoose = require('mongoose');
var passport = require('passport');
var User = mongoose.model('User');

exports.listAllUsers = function(req, res) {
  User.find({}, function(err, user) {
    res.json(user);
    res.status(200);
      
    if (err) {
      res.status(500);
      console.log(err)
    }
  });
};


exports.createUser = function(req, res) {
    var newUser = new User(req.body);
    var pass = req.body.password;
  //  console.log("body: " + JSON.stringify(req.body));
  //  console.log("params: " + JSON.stringify(req.params));
 //   console.log("user: " + JSON.stringify(newUser));

  console.log("searching for username: " + newUser.username );
    User.findOne(
      { 'username': newUser.username }, function(err1, user) {
      
        if (user) {
          console.log("username already exists!: " + newUser.username);
          res.status(403);
          res.json({
            "error_message" : "username already exists in db!",
          });
        }else{
          console.log("trying to create user: "+ newUser.username);
          newUser.setPassword(pass);
          newUser.save(function(err, user) {

            if (err) {
              res.status(500);
              console.log(err)
            }else{
              user = user.toObject();
              delete user.password;
              delete user.salt;
              res.json(user);
              res.status(200);
                
              console.log("User Successfully Added!");
            }

          });
        }
        
      }
    )
    
  };

exports.deleteUser = function(req, res) {
  User.deleteOne(
    { 'username': req.params.username }, function(err, user) {
    
      if (err) {
        res.status(500);
        console.log(err)
      }else{
        user = user.toObject();
        delete user.password;
        delete user.salt;
        res.json(user);
        res.status(200);
        console.log("User Successfully Deleted");
      }  
  
    }
  )
}

exports.login = function(req, res) {

  passport.authenticate('local', function(err, user, info){
  var token;
    if (err) {
      res.status(404).json(err);
      return;
    }

    if(user){
      token = user.generateJwt();
      user = user.toObject();
      delete user.password;
      delete user.salt;
      res.status(200);
      res.json({
        "token" : token,
        "user": user
      });
    } else {
      res.status(401).json(info);
    }
  })(req, res);

};

exports.logout = function(req, res) {
  console.log("should send log out instruction to wipe token");
  res.sendStatus(200);
};

    
exports.getUser = function(req, res) {

  User.findOne(
    { '_id': req.params.id }, function(err, user) {
     
      if (err) {
        res.status(500);
        console.log(err);
      }else{
        user = user.toObject();
        delete user.password;
        delete user.salt;
        res.json(user);
        res.status(200);
      }
  
    }).populate({ path: 'coursework', populate: { path: 'milestones' } })
};

exports.getUserObj = function(id, res) {
  
  User.findOne(
    { '_id': id }, function(err, user) {
    
      console.log('user Obj ' + JSON.stringify(user))

      res(user);

      if (err) {
        res.status(500);
        console.log(err);
      }
      
      console.log("Found user with username: " + user.username);
  
    })
};