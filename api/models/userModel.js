'use strict';
var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var Schema = mongoose.Schema;

var UserSchema = new Schema(
{
  username: String,
  password: String,
  coursework: [
    { type: mongoose.Schema.Types.ObjectId, ref: 'Courseworks' }
  ],
  salt: String
  
},
  {
    versionKey: false,
    timestamps: true  
});


UserSchema.methods.setPassword = function(passwordArg) { 
    this.salt = crypto.randomBytes(16).toString('hex'); 
    this.password = crypto.pbkdf2Sync(passwordArg, this.salt, 1000, 64, `sha512`).toString(`hex`); 
 };


 UserSchema.methods.validPassword = function(passwordArg) { 
  var password = crypto.pbkdf2Sync(passwordArg, this.salt, 1000, 64, `sha512`).toString(`hex`); 
  // console.log("comparing password provided: " + password + " \n to the stored user: " + this.password);
  return this.password === password; 
}; 

UserSchema.methods.generateJwt = function() {

  return jwt.sign({
    _id: this._id,
    username: this.username,
  }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '60 minutes' }); 

};

module.exports = mongoose.model('User', UserSchema,'User');