'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var CourseworkSchema = new Schema(
{
  dueDate: Date,
  actualCompletionDate: Date,
  title: String,
  module: String,
  milestones: [
    { type: mongoose.Schema.Types.ObjectId, ref: 'Milestones' }
  ],
  user: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User' 
  }
  
},
  {
    versionKey: false,
    timestamps: true  
});

module.exports = mongoose.model('Courseworks', CourseworkSchema);