'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var MilestoneSchema = new Schema(
{
  name: String,
  targetDate: Date,
  description: String,
  coursework: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Courseworks' 
  },
  complete:  {
    type: String,
    enum : ['IN PROGRESS', 'NOT STARTED','COMPLETE'],
    default: 'NOT STARTED'
  },
  
},
  {
    versionKey: false,
    timestamps: true  
});

module.exports = mongoose.model('Milestones', MilestoneSchema);