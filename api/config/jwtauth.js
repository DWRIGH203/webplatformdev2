var UserModel = require('../models/userModel');
var jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {

  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['access_token'];

  if (token) {
  //  console.log("token found!: " + token);
    try {

      jwt.verify(token, process.env.ACCESS_TOKEN_SECRET , (err,user) => {
        if(err) {
          console.log("error: " + err)
         return res.sendStatus(403)
        }
        req.user = user
        next()
      })
  
    } catch (err) {
      console.log("an error occurred when trying to verify the token!")
      return res.sendStatus(500);
    }
  } else {
    console.log("NO TOKEN FOUND!");
    return res.sendStatus(401);
  }
};