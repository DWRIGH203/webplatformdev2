var passport = require('passport');
var LocalStrategy  = require('passport-local').Strategy;
var bcrypt = require('bcrypt');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var userController = require('../controllers/userController');

passport.use(new LocalStrategy(
  function(username, password, done) {
    console.log("searching for username:" + username)
    User.findOne(
      { 'username': username }
      , function (err, user){
      if (err) { return done(err); }
      if (!user) { return done(null, false,{ message: 'User not found'}); }

  //    console.log("user found: " + JSON.stringify(user))

      if(user.validPassword(password)){
        console.log("successfully authenticated user: "+ username);
        done(null,user);
      } else{
        console.log("incorrect details passed for user: "+ username);
        done(null, false);
      }

    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});
passport.deserializeUser(function(id, done) {
    User.findOne({
        _id: req.params.id
    }, '-password -salt', function(err, user) {
        done(err, user);
    });
});
