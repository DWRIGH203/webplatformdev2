var express = require('express'),
  app = express();

app.use(express.static('./dist/coursework-app'));

app.get('/*', function(req, res) {
          res.sendFile('index.html', {root: 'dist/coursework-app/'});
        });
port = process.env.PORT || 8080;
app.listen(port);
console.log("front end started on : " + port);