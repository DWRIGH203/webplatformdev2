export const environment = {
  production: false,
  url : 'https://web-dev-2-project.herokuapp.com',
  coursework: '/api/coursework',
  courseworkById: '/api/coursework/:id',
  deleteCoursework: '/api/deleteAllCourseworks',
  milestone: '/api/milestone',
  milestoneById: '/api/mileston/:id',
  deleteMilestone: '/api/deleteAllMilestones'
};