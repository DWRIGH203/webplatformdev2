import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuardService } from './services/auth-guard.service';
import { Coursework } from './models/Coursework';
import { IndivdualCourseworkComponent } from './components/indivdual-coursework/indivdual-coursework.component';
import { AuthenticationService } from './services/authentication.service';


const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuardService]},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent},
  { path: 'home-page', component: HomeComponent, canActivate: [AuthGuardService]},
  { path: 'coursework', component: IndivdualCourseworkComponent, canActivate: [AuthGuardService]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
