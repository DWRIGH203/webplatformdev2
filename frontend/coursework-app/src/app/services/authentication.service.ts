import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../models/User';
import { environment } from '../../environments/environment';


interface TokenResponse {
  token: string,
  user: any
}

export interface TokenPayload {
  username: String, 
  password: String, 
}



@Injectable()
export class AuthenticationService {
  private token: string;
  private username: String;
  private userId;
  private user; 

  constructor(private http: HttpClient, private router: Router) {}

  private saveToken(token: string): void {
    console.log('saving token: ' + token);
    localStorage.setItem('json-token', token);
    this.token = token;
  }

  
  // private saveToken(token: string): void {
  //   console.log('saving token: ' + token);
  //   this.token = token;
  // }

  public getUserId() {
    return this.userId;
  }

  public setUser(user) {
    localStorage.setItem('user', user)
    this.user = user;
  }

  // public getUser() {
  //   return this.user; 
  // }

  public setUserId(userId) {
    this.userId = userId;
  }

  // public getToken(): string {
  //   return this.token;
  // }

  public getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('json-token');
    }
    return this.token;
  }


  public getUserDetails(): User {
    const token = this.getToken();
    let payload;
    console.log('Logging token - ' + token);
    if (token !== null && token !== 'undefined' && token !== '' && token !== undefined) {
      payload = token.split('.')[1];
      payload = window.atob(payload);
      return JSON.parse(payload);
    } else {
      return null;
    }
  }

  public isLoggedIn(): boolean {
    const user = this.getUserDetails();
    if (user) {
      return user.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }
  
   makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
 
  public register(user: TokenPayload): Observable<any> {
    const userToSend = {
      username: user.username,
      password: user.password
    };

    console.log('Registering User');
    
    return this.http.post(environment.url+'/api/createUser', userToSend);
  }

  public login(user: TokenPayload): Observable<any> {
    console.log('Logging in to system. ');
    let response;
    // return this.request('post', 'login', user);
     response = this.http.post(environment.url+'/api/login', user);
    //  console.log('The login return', response);
    response = response.pipe(
      map((data: TokenResponse) => {
        if(data) {
          console.log('Saving Token ' + data.token);
          console.log('Saving user', data.user);
          this.setUser(data.user);
          this.saveToken(data.token);
          this.setUserId(data.user._id);
        }
        return data;
      })
    );
    return response;

  }

  public profile(): Observable<any> {
    console.log('Getting Profile');
    // return this.request('get', 'profile');
    return this.http.get('/api/profile', { headers: { Authorization: `Bearer ${this.getToken()}` }});

  }

  public logout(): void {
    this.token = '';
    console.log('Removing token');
    window.localStorage.removeItem('json-token');
    console.log('Navigating to login');
    this.router.navigateByUrl('/login');
  }

  public getAllUsers(): Observable<any> {
    return this.http.get('/api/get-all-users', { headers: { Authorization: `Bearer ${this.getToken()}` }});
  }

  public removeUser(userId): Observable<any> {
    return this.http.post('/api/remove-user',{userId}, { headers: { Authorization: `Bearer ${this.getToken()}` }});
  }

  public getUsername() {
    return this.username;
  }

  public setUsername(username:String) {
    this.username = username;
  }

  public getUserByUsername(username){
     
    const response = this.http.get(environment.url + environment.user + username, {headers: {access_token: this.getToken()}});    
    console.log('Get User By Username', response); 
    return response; 
  }

  public getUserObj() {
    return this.user;
  }


}
