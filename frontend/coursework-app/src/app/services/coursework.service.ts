import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Coursework } from '../models/Coursework';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CourseworkService {

  constructor(private http: HttpClient, private auth: AuthenticationService) { }

  getAllCourseworks() {
    return this.http.get<Coursework[]>(environment.url + environment.coursework, { headers: {access_token: this.auth.getToken()}});
  }

  addCoursework(coursework) {

    return this.http.post<Coursework>(environment.url + environment.coursework, {coursework, access_token: this.auth.getToken()});
  }

  getCourseworkById(id: string ) {
    return this.http.get<Coursework>(environment.url + environment.courseworkById + id, { headers: {access_token: this.auth.getToken()}});
  }

  editCourseworkById(id: string, coursework) {  
    console.log('The coursework being sent for update', coursework);
    return this.http.put<Coursework>(environment.url + environment.courseworkById + id, {coursework, access_token: this.auth.getToken()});
  }

  deleteCourseworkById(id: string) {
    return this.http.delete<Coursework>(environment.url + environment.courseworkById + id, {headers: {access_token: this.auth.getToken()}});
  }

  // getCourseworkForUser() {
  //   return this.auth.getUser();
  // }

}
