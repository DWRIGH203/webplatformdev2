import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Milestone } from '../models/Milestone';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class MilestoneService {

  constructor(private http: HttpClient, private auth: AuthenticationService) { }

  getAllMilestonesForCoursework() {
    return this.http.get<Milestone[]>(environment.url + environment.milestone, { headers: {access_token: this.auth.getToken()}});
  }

  addMilestone(milestone) {
    return this.http.post<Milestone>(environment.url + environment.milestone, {milestone, access_token: this.auth.getToken()});
  }

  editMilestoneById(id: string, milestone) {
    return this.http.put<Milestone>(environment.url + environment.milestoneById + id, {milestone, access_token: this.auth.getToken()});
  }

  deleteMilestoneById(id: string) {
    return this.http.delete<Milestone>(environment.url + environment.milestoneById + id, { headers: {access_token: this.auth.getToken()}});
  }

  deleteAllMilestones() {
    return this.http.delete<Milestone>(environment.url + environment.deleteMilestone);
  }

  
}
