import { TestBed } from '@angular/core/testing';

import { CourseworkService } from './coursework.service';
import { HttpClientModule } from '@angular/common/http';

describe('CourseworkService', () => {
  let service: CourseworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ]
    });
    service = TestBed.inject(CourseworkService);
  });

  it('Coursework Service should be created', () => {
    expect(service).toBeTruthy();
  });
});
