export class Milestone {
    _id: string;
    name: string;
    targetDate: string;
    description: string;
    coursework: String;
    modifiedId: string;
    complete: string;
}