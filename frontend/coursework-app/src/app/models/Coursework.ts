import { Milestone } from './Milestone';

export class Coursework {
    dueDate: Date;
    actualCompletionDate: Date;
    title: string;
    module: string;
    milestones: Milestone[];
    user: string;
}
