import { Component, OnInit } from '@angular/core';
import { TokenPayload, AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: TokenPayload = {
    username: '',
    password: '',
  };

  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    console.log('Sending these creds', this.credentials)
    this.auth.login(this.credentials).subscribe(() => {
      this.auth.setUsername(this.credentials.username);
      this.router.navigateByUrl('/home-page');
    }, (err) => {
      console.error(err);
      alert('Credentials were wrong couldn\'t contact the server');
    });
  }

}
