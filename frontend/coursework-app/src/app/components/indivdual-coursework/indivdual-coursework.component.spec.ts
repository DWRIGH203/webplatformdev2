import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndivdualCourseworkComponent } from './indivdual-coursework.component';

describe('IndivdualCourseworkComponent', () => {
  let component: IndivdualCourseworkComponent;
  let fixture: ComponentFixture<IndivdualCourseworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndivdualCourseworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndivdualCourseworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
