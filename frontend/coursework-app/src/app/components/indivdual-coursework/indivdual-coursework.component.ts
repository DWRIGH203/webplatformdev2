import { Component, OnInit } from '@angular/core';
import { CourseworkService } from 'src/app/services/coursework.service';
import { Coursework } from 'src/app/models/Coursework';
import { MilestoneService } from 'src/app/services/milestone.service';
import { Milestone } from 'src/app/models/Milestone';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { createPopper } from '@popperjs/core';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-indivdual-coursework',
  templateUrl: './indivdual-coursework.component.html',
  styleUrls: ['./indivdual-coursework.component.css']
})
export class IndivdualCourseworkComponent implements OnInit {

  constructor(
    private courseworkService: CourseworkService,
    private milestoneService: MilestoneService,
    private _ActivatedRoute: ActivatedRoute,
    private modalService: NgbModal, 
    ) { }

  coursework: Coursework;
  milestones = [];
  courseworkId: string; 
  sharePopup = false; 
  closeResult = '';
  courseworkURL = "";
  IN_PROGRESS = "IN_PROGRESS";
  listOfStatuses = ['IN PROGRESS', 'COMPLETE', 'NOT STARTED']
  dateFormat = 'dd/MM/yyyy';

  milestoneForm = {
    milestoneName: '',
    milestoneDate: '',
    milestoneDescription: '',
    milestoneCompleted: '',
    coursework: '',
  }

  ngOnInit(): void {
    this.courseworkId = this._ActivatedRoute.snapshot.queryParamMap.get("_id");
    this.getCourseworkById(this.courseworkId);
    this.courseworkURL = environment.frontendUrl + "/coursework?_id=" + this.courseworkId;
  }

  public getCourseworkById(courseworkId): void {
    this.courseworkService.getCourseworkById(courseworkId).subscribe( response => {
      this.coursework = response[0];
      this.milestones = response[0].milestones
      // if(this.coursework.actualCompletionDate){
      //   this.coursework.actualCompletionDate = this.stripDate(this.coursework.actualCompletionDate);
      // }
      // if(this.coursework.dueDate){
      //   this.coursework.dueDate = this.stripDate(this.coursework.dueDate);
      // }
    }, error => {
      console.log("Error Getting CourseworkById");
    })
  }
  
  public createMilestone(): void {
    this.milestoneForm.coursework = this.courseworkId;
 
   const milestone = {
      name: this.milestoneForm.milestoneName,
      targetDate: this.milestoneForm.milestoneDate,
      description: this.milestoneForm.milestoneDescription,
      complete: this.milestoneForm.milestoneCompleted,
      coursework: this.courseworkId,
    }

    if(this.milestoneForm.milestoneCompleted.length > 0) {
      milestone.complete = this.milestoneForm.milestoneCompleted[0];
    }
    else {
      milestone.complete = "NOT STARTED"
    }

    this.milestoneService.addMilestone(milestone).subscribe( response => {
      this.getCourseworkById(this.courseworkId);
    }, error => {
    })
  }

  copyCourseworkURL() {
      var tempInput = document.createElement("input");
      tempInput.style.position = "absolute";
      tempInput.style.left = "-1000px";
      tempInput.style.top = "top: -1000px";
      tempInput.value = this.courseworkURL
      document.body.appendChild(tempInput);
      tempInput.select();
      document.execCommand("copy");
      document.body.removeChild(tempInput); 
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public deleteMilestoneById(id: string) {
    this.milestoneService.deleteMilestoneById(id).subscribe(
      response => {
        this.getCourseworkById(this.courseworkId);
      }, error => {
        console.log("Error Deleting Milestone")
      }
    )
  }

  public changeMilestoneStatus(newStatus: any, index) {
    this.coursework.milestones[index].complete = newStatus;
    this.editMilestoneById(this.coursework.milestones[index]._id, index)
    }

  public updateCoursework() {
    this.courseworkService.editCourseworkById(this.courseworkId, this.coursework).subscribe(
      response => {
        this.getCourseworkById(this.courseworkId);
      }, error => { 
        console.log('Error updating coursework', error)
       }
    )

  }

  public editMilestoneById(id, index) {
    this.milestoneService.editMilestoneById(id, this.coursework.milestones[index]).subscribe(
      response => {
        this.getCourseworkById(this.courseworkId);
      }, error => { 
       }
    );
  }

  public stripDate(date: Date): Date{
    const dateString = date.toString();
    return new Date(dateString.substring(0, dateString.indexOf('T')));
  }

}
