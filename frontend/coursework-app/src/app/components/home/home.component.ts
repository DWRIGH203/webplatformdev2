import { Component, OnInit, ViewChild } from '@angular/core';
import { CourseworkService } from 'src/app/services/coursework.service';
import { Coursework } from 'src/app/models/Coursework';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {
  allCourseworks;
  username:String;
  closeResult = '';
  todaysDate = new Date();
  dateInSixMonths = new Date(this.todaysDate.setMonth(this.todaysDate.getMonth() + 6))
  newCoursework: Coursework = {
    dueDate: this.dateInSixMonths,
    actualCompletionDate: null,
    title: '',
    module: '',
    milestones: [],
    user: this.authenticationService.getUserDetails()._id,
  };
  dateFormat = 'dd/MM/yyyy';
  constructor(private courseworkService: CourseworkService, private authenticationService: AuthenticationService, private router: Router, private modalService: NgbModal) { }
  
  ngOnInit(): void {
    this.getCourseworks();
    this.username = this.authenticationService.getUserDetails().username;
    document.getElementById('viewAll').style.border = 'solid';
    document.getElementById('viewAll').style.borderWidth = 'thick';
    }

  public getCourseworks(): void {
    this.authenticationService.getUserByUsername(this.authenticationService.getUserDetails()._id).subscribe(user => {
      console.log('User Info from Get Courseworks method', user)
      this.allCourseworks = user; 
      this.allCourseworks = this.allCourseworks.coursework;
      this.authenticationService.setUser(user);
    });
  }

  public addCoursework(coursework: Coursework): void {
    console.log('coursework being created', coursework)
    this.courseworkService.addCoursework(coursework).subscribe( 
      response => {
      console.log("Success Adding Coursework");
      this.newCoursework = {
        dueDate: this.dateInSixMonths,
        actualCompletionDate: null,
        title: '',
        module: '',
        milestones: [],
        user: this.authenticationService.getUserDetails()._id,
      };
      this.getCourseworks();
    }, error => {
      console.log("Error Adding Coursework");
    },
    () => {
      this.getCourseworks();
    }
    );
  }

  // public editCourseworkById(courseworkId): void {
  //   this.courseworkService.editCourseworkById(courseworkId).subscribe( response => {
  //     console.log("Success Editing Coursework");
  //   }, error => {
  //     console.log("Error Editing Coursework");
  //   })
  // }

  public deleteCourseworkById(courseworkId): void {
    this.courseworkService.deleteCourseworkById(courseworkId).subscribe( response => {
      console.log("Success Deleting CourseworkById", response);
      this.getCourseworks();
    }, error => {
      console.log("Error Deleting CourseworkById", error);
    })
  }

  public logout() {
    this.authenticationService.logout();
  }

  public viewCoursework(id: string) {
    this.router.navigate(['/coursework'], { queryParams: { _id: id } });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    });
  }


  listIncomplete(){
    let incompleteCourseworks = []

    this.allCourseworks = this.allCourseworks.forEach( coursework => {
      if(coursework.actualCompletionDate === null || coursework.actualCompletionDate === undefined || coursework.actualCompletionDate === ''){
        incompleteCourseworks.push(coursework);
      }
    });


    this.allCourseworks = incompleteCourseworks;
  }

  viewAll(){
    this.getCourseworks();
  }

} 
