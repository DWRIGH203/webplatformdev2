import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  
  @Output() addCourseworkPopup = new EventEmitter<any>();
  @Output() viewAllCourework = new EventEmitter<any>();
  @Output() viewIncompleteCoursework = new EventEmitter<any>()
  @Output() addMilestonePop = new EventEmitter<any>();
  @Output() shareLinkPopup = new EventEmitter<any>()
  @Input() isHomePage;
  @Input() isCourseworkPage;

  

  constructor(private authenticationService: AuthenticationService, private modalService: NgbModal) { }
  ngOnInit(): void {
  }

  addCoursework() {
    this.addCourseworkPopup.emit();   
  }

  viewIncomplete() {
    this.viewIncompleteCoursework.emit();   
  }

  viewAll() {
    this.viewAllCourework.emit();   
  }

  addMilestone() {
    this.addMilestonePop.emit();   
  }

  share() {
    this.shareLinkPopup.emit();   
  }
  public logout() {
    this.authenticationService.logout();
  }

}
