require('dotenv').config()
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  bodyParser = require('body-parser');
  const cors = require("cors");
  app.use(cors());

  const mongoDbOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }

  
try{
  // mongoose instance connection url connection
  mongoose.Promise = global.Promise;
  mongoose.connect('mongodb://web-dev-user:technoteam100@ds161894.mlab.com:61894/heroku_w8w38mv5',mongoDbOptions); 
}catch{
  console.log("unable to connect to mongoDB instance so exiting!");
  process.exit(1);
}

var passport = require('passport')
  require('./api/models/userModel')
  require('./api/models/milestoneModel')
  require('./api/models/courseworkModel')
  require('./api/config/passport');
app.use(passport.initialize());
app.use(passport.session());


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var courseworkRoute = require('./api/routes/courseworkRoutes' ); //importing route
var userRoute = require('./api/routes/userRoute');
var appRoute = require( './api/routes/appRoute');
var milestoneRoute = require('./api/routes/milestoneRoutes');

courseworkRoute(app);
userRoute(app); //register the route
appRoute(app);
milestoneRoute(app);

app.listen(port);


console.log('API server started on: ' + port);