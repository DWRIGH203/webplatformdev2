# WebPlatformDev2

#Build

## BACKEND

To start the backend server, ensure you are in the 'webplatformdev2' folder and enter command: node server.js

The server should either be allocated a port by the OS or start on port 3000. This will be confirmed in the output in the terminal. 

Requests can then be made to query the backend if necessary via: http://localhost:<PORT>/api/

## FRONTEND
The front end code is also within this repository and instructions on how to run this app can be found within the project at path: frontend/coursework-app/README.md

