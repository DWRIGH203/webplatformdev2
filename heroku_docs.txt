Steps taken to get backend and front end into heroku:

## BACKEND

- updated start to use node to run rather than nodemon
- updated node version to at least node v10 to fix crypto issue
- removed swagger from dependency

## FRONT END 

- Added preinstall step to install the angular cli before running npm install 
- played about with memory allocation as received out of memory error regular (may not be an issue now)
- Adding frontend server to route requests through
- Updated proc file so git push knows how to start app
- Updated calls to backend to use back end api hosted in heroku

## NOTES FOR PUSHING UI TO heroku
url :https://medium.com/@shalandy/deploy-git-subdirectory-to-heroku-ea05e95fce1f

 - a line may need added to the package.json file for the angular project. currently stored in _comment but it needs to be its own value within the scripts section

 - npm install -g heroku
 - heroku login 
(SHOULD BE REDIRECTED TO BROWSER TO LOG IN)
 - heroku git:remote -a web-dev-2-front-end-app
 - cd to webplatformdev2
 - git subtree push --prefix frontend/coursework-app heroku master
 (THIS SHOULD TAKE ABOUT 5 MINS TO RUN AND DEPLOY)

 ##IMPORTANT TO CHECK FOR EACH APP - LOG LOCATIONS
 - https://dashboard.heroku.com/apps/web-dev-2-front-end-app/logs
 - https://dashboard.heroku.com/apps/web-dev-2-project/logs 


url for creating server:
https://medium.com/better-programming/how-to-deploy-your-angular-9-app-to-heroku-in-minutes-51d171c2f0d



